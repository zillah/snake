// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "FoodSpawner.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Step = 100.0f;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 0.5f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	AddSnakeElement(4);
	Points = 0;
}
// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int Num)
{
	for (int i = 0; i < Num; ++i) 
	{
		FVector NewLocation(SnakeElements.Num() * Step, 0, 0);
		FTransform NewTransform = FTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		Points += PointsToAdd;
		if (ElementIndex == 0)
			NewSnakeElement->SetHeadType();
		else
			NewSnakeElement->MeshComponent->SetVisibility(false);
		MovementSpeed = FMath::Clamp(MovementSpeed - 0.01, 0.3, 0.5);
		SetActorTickInterval(MovementSpeed);
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += Step;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= Step;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += Step;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= Step;
		break;
	}
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		FVector PreviousLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousLocation);
		CurrentElement->MeshComponent->SetVisibility(true);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::KillSnake()
{
	for (auto Element : SnakeElements)
		Element->Destroy();
	Destroy();
}

void ASnakeBase::SpawnNewFood()
{
	SpawnerActor->SpawnFood();
}