// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFoodSpawner;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};


UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float Step;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(BlueprintReadWrite)
	AFoodSpawner* SpawnerActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Points = 0;

	UPROPERTY(EditDefaultsOnly)
		int32 PointsToAdd = 5;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int Num = 1);
	void Move();
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	void KillSnake();
	void SpawnNewFood();
};
